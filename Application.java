public class Application {
    public static void main(String[] args) {
        Student student1 = new Student(17, "Ashley", "Coding", 'F', new int[] {89, 90, 79, 86});
        Student student2 = new Student(19, "Lee", "Cooking", 'M', new int[] {78, 90, 70, 63, 86});
        Student student3 = new Student(18, "Kei", "Gaming", 'M', new int[] {69, 98, 76, 88});
        Student student4 = new Student(18, "Ann", "Reading", 'F', new int[] {67, 56, 82, 68, 88});
	
        System.out.println(student1.reverseName());
        System.out.println(student2.printHobby());
        System.out.println(student3.multiplyAge());
        System.out.println(student3.amountLearnt());
        System.out.println("Average grade for " + student4.name + ": " + student4.calculateGradeAverage());
    }
}
