public class Student {
    int age;
    String name;
    String hobby;
    char gender;
    int[] grades;
	int amountLearnt;

    public Student(int age, String name, String hobby, char gender, int[] grades, int amountLearnt) {
        this.age = age;
        this.name = name;
        this.hobby = hobby;
        this.gender = gender;
        this.grades = grades;
		this.amountLearnt = amountLearnt;
    }

    public String reverseName() {
        String reverse = "";
        for (int i = name.length() - 1; i >= 0; i--) {
            reverse += name.charAt(i);
        }
        return reverse + " is the reverse of " + name;
    }

    public String multiplyAge() {
        return (age * 2) + " is the double of " + name + "'s age";
    }

    public String printHobby() {
        return name + "'s favorite hobby: " + hobby;
    }

    public double calculateGradeAverage() {
        double gradeSum = 0;
        for (int i = 0; i < grades.length; i++) {
            gradeSum += grades[i];
        }
        return gradeSum / grades.length;
    }
}
